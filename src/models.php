<?php 

/**
 * Cette page php sert à expliciter les liens entre les tables de la bdd pour utiliser Paris/idiorm
 */

class Users extends Model {
    public static $_table = 'users';

    public function Categorie() {
        return $this->belongs_to('Categorie', 'categorie_id');
    }
}

class Notation extends Model {
    public static $_table = 'notation';

    public function Age_ccpg() {
        return $this->belongs_to('Age_ccpg', 'age_ccpg_id');
    }

    public function Age_ccps() {
        return $this->belongs_to('Age_ccps', 'age_ccps_id');
    }
}

class Categorie extends Model {
    public static $_table = 'categorie';
}

class Age_ccpg extends Model {
    public static $_table = 'age_ccpg';
}

class Age_ccps extends Model {
    public static $_table = 'age_ccps';
}

class F2400 extends Model {

}

class H2400 extends Model {

}

class Marche_coursef extends Model {

}

class Marche_courseh extends Model {

}

class Natationf extends Model {

}

class Natationh extends Model {

}

class Pompesf extends Model {

}

class Pompesh extends Model {
    
}