<?php require "vendor/autoload.php";

use \Tamtamchik\SimpleFlash\Flash;
use \Respect\Validation\Exceptions\NestedValidationException;

session_start();

$loader = new Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/views');
$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig\Extension\DebugExtension()); // Add the debug extension
    $twig->addFunction(new Twig\TwigFunction('messages', function () { //add flash message
        return Flash::display();
    }));

});

Flight::map('render', function ($template, $data = array()) {
    Flight::view()->display($template, $data);
});

Flight::before('start', function (&$params, &$output) {
    ORM::configure('sqlite:notation.sqlite3');
});

Flight::route('/', function() {
    index();
});

Flight::route('/adduser(/@id)', function($id){
    adduser($id);
});

Flight::route('/deluser/@id', function($id){
    if($id) {
        $user = Model::factory('Users')->find_one($id);
        $notation = Model::factory('Notation')->where('user_id', $id)->find_many();
        foreach($notation as $not) {
            $not->delete();
        }
        $user->delete();
    }
    Flash::success('L\'utilisateur a bien été supprimé');
    Flight::redirect('/userstab');
});

Flight::route('/profile/@id', function($id){
    profile($id);
});

Flight::route('/notation/@useri(/@ntid)', function($userid, $ntid){
    notation($userid, $ntid);
});

Flight::route('/delnotation/@id/@user_id', function($id, $user_id){
    $notation = Model::factory('Notation')->find_one($id);
    $notation->delete();
    Flight::redirect('/profile/'.$user_id);
});

Flight::route('/userstab', function(){
    userstab();
});

Flight::start();